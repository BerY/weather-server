var express      = require('express');
var app          = express();


var bodyParser   = require('body-parser');
var morgan       = require('morgan');
var mongoose     = require('mongoose');
var passport  	 = require('passport');
var jwt          = require('jwt-simple');
var cors 	       = require('cors');
var port         = process.env.PORT || 8080;

var config       = require('./config/database'); 
var User         = require('./app/models/user'); 
var Temperatures = require('./app/models/temperatures'); 
var Settings     = require('./app/models/settings'); 
var Raspberry    = require('./app/models/raspberry');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
 
app.use(morgan('dev'));
 
app.use(passport.initialize());
 
mongoose.connect(config.database);
 
require('./config/passport')(passport);
 
var originsWhitelist = [
  'http://localhost:4200'    //this is my front-end url for development
];

var corsOptions = {
  origin: function(origin, callback){
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
  },
  credentials: true
}

app.use(cors(corsOptions)); 

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}; 

preProcess = function(req, res, cb) {
  var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      name: decoded.name
    }, function(err, user) {
        if (err) throw err;
 
        if (!user) {
          return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
          return cb(req, res);
        }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
}

// bundle our routes
var apiRoutes = express.Router();

apiRoutes.post('/signup', function(req, res) {
  if (!req.body.name || !req.body.password) {
    res.json({success: false, msg: 'Please pass name and password.'});
  } else {
    var newUser = new User({
      name: req.body.name,
      password: req.body.password
    });
    // save the user
    newUser.save(function(err) {
      if (err) {
        return res.json({success: false, msg: 'Username already exists.'});
      }
      res.json({success: true, msg: 'Successful created new user.'});
    });
  }
});

apiRoutes.post('/authenticate', function(req, res) {
  
  User.findOne({
    name: req.body.name
  }, function(err, user) {
    if (err) throw err;
 
    if (!user) {
      res.send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      // check if password matches
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          var token = jwt.encode(user, config.secret);
          // return the information including token as JSON
          res.json({success: true, auth_token: 'JWT ' + token});
        } else {
          res.send({success: false, msg: 'Authentication failed. Wrong password.'});
        }
      });
    }
  });
});

apiRoutes.get('/current', passport.authenticate('jwt', { session: false}), function(req, res) {
  preProcess(req, res, function(req, res){
    Raspberry.getTemperature(function(e, data){
      if (e) throw e;
      
      res.contentType('application/json');
      res.send(JSON.stringify({temp: data.temp}));
    })
  })
});

apiRoutes.get('/stats', passport.authenticate('jwt', { session: false}), function(req, res) {
  preProcess(req, res, function(req, res){
    Temperatures.getStats(req.query.range, function(e, data){
      if (e) throw e;
      
      res.contentType('application/json');
      res.send(JSON.stringify(data));
    });
  })
});

apiRoutes.get('/chart', passport.authenticate('jwt', { session: false}), function(req, res) {

  preProcess(req, res, function(req, res){
    Temperatures.getChart(req.query.range, function(e, data){
      if (e) throw e;

      res.contentType('application/json');
      res.send(JSON.stringify(data));
    });
  })
});

apiRoutes.get('/settings', passport.authenticate('jwt', { session: false}), function(req, res) {
  preProcess(req, res, function(req, res){
    Settings.findOne({}, function(e, data){
      res.contentType('application/json');
	    res.send(JSON.stringify({email: data.email, send: data.send, min: data.min, max: data.max}));
    })
	 })
});

apiRoutes.post('/settings', passport.authenticate('jwt', { session: false}), function(req, res) {
  preProcess(req, res, function(req, res){   
    Settings.updateSettings(req, function(err) {
      if (err) {
        console.log(err);
      }
    });
  })
});

apiRoutes.post('/settings/clean', passport.authenticate('jwt', { session: false}), function(req, res) {
  preProcess(req, res, function(req, res){   
    Temperatures.clean(req, function(err) {
      if (err) {
        console.log(err);
      }
    });
  })
});

apiRoutes.post('/profile', passport.authenticate('jwt', { session: false}), function(req, res) {
  preProcess(req, res, function(req, res){

  });
});

app.use('/api', apiRoutes);
app.listen(port);

console.log('There will be dragons: http://localhost:' + port);