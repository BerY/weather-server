var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
 
var TemperaturesSchema = new Schema({
  temp: {
        type: Number,
        unique: true,
        required: true
    },
  date: {
        type: Date,
        required: true
    }
});
  
TemperaturesSchema.statics.clean = function (range, cb) {
  this.remove({}, cb);
}

TemperaturesSchema.statics.getStats = function (range, cb) {
    
  var lim = range * 1;

  return this.aggregate(
  [
    { 
      $project:
      {	
        year: { $year: "$date" },
        month: { $month: "$date" },
        day: { $dayOfMonth: "$date" },
        hour: { $hour: "$date" },
        temp: 1
      }
   	},
    {
      $group:
      {
         "_id": {year: "$year", month: "$month", day: "$day", hour: "$hour" },
         "temp": { "$avg": "$temp" },
	     "min" : { "$min": "$temp" },
	     "max" : { "$max": "$temp" }
 	   }
   	 },
	 {
	   $limit : lim
   	 },
     {
       $group:
       {
         "_id": null,
         "avg": { "$avg": "$temp" },
	     "min" : { "$min": "$temp" },
	     "max" : { "$max": "$temp" }
       }
    },
  ], cb);
};
 
TemperaturesSchema.statics.getChart = function (range, cb) {

     var lim = range;
     if ( range == 1 || range == 2)
	lim = lim * 24;
     else
	lim = lim * 1;

     if ( range == 1 || range == 2)
	return 	this.aggregate([
    	{ 
          $project:
          {	
           year: { $year: "$date" },
           month: { $month: "$date" },
           day: { $dayOfMonth: "$date" },
           hour: { $hour: "$date" },
           temp: 1
       	  }
   	},
    	{
         "$group": {
             "_id": {year: "$year", month: "$month", day: "$day", hour: "$hour" },
             "temp": { "$avg": "$temp" },
	     "min" : { "$min": "$temp" },
	     "max" : { "$max": "$temp" }
        	 }
   	 },
	 {
	   "$limit" : lim
   	 },
	 { $group: {  "_id" : null, "data" : { "$push" :"$temp" } } },
	 { $project: { data: "$data" }}
	], cb);
      
      return 	this.aggregate([
    	{ 
          $project:
          {	
           year: { $year: "$date" },
           month: { $month: "$date" },
           day: { $dayOfMonth: "$date" },
           temp: 1
       	  }
   	},
    	{
         "$group": {
             "_id": {year: "$year", month: "$month", day: "$day" },
             "temp": { "$avg": "$temp" },
	     "min" : { "$min": "$temp" },
	     "max" : { "$max": "$temp" }
        	 }
   	 },
	 {
	   "$limit" : lim
   	 },
	 { $group: {  "_id" : null, "data" : { "$push" :"$temp" } } },
	 { $project: { data: "$data" }}
	], cb);
 
	
};
 
module.exports = mongoose.model('Temperatures', TemperaturesSchema)
