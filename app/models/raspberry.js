var mongoose = require('mongoose').set('debug', true);
var bcrypt = require('bcrypt');
 
var Schema = mongoose.Schema;

var RaspberrySchema = new Schema({
  temp: {
    type: Number,
    required: true
  }
});

RaspberrySchema.statics.getTemperature = function (cb) {
	return this.findOne({}, cb);
};
 
module.exports = mongoose.model('Raspberry', RaspberrySchema)