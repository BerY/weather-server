var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
 
var Schema = mongoose.Schema;

var SettingsSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  send: {
    type: Boolean,
    required: true
  },
  min: {
    type: Number,
    required: true
  },
  max: {
    type: Number,
    required: true
  }
});


SettingsSchema.statics.updateSettings = function (req, cb) {

    var sets = {
      email: req.body.email,
      send: req.body.send,
      min: req.body.min,
      max: req.body.max
    };

    this.findOneAndUpdate([], sets, cb);
};
 
SettingsSchema.set('collection', 'settings');

module.exports = mongoose.model('Settings', SettingsSchema)
